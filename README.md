# README #
## Overview ##
snap is a customizable flight stack for quadrotors equipped with Qualcomm's Snapdragon Flight board. The position, velocity, attitude, and IMU biases are estimated by fusing propogated IMU measurments with an external measurement (i.e. from MoCap, VIO, etc.) via a Kalman filter. Attitude control is achieved through a quaternion-based sliding controller. Snap uses a [PWM library](https://bitbucket.org/brettlopez/snap-dspal/overview) that enables the Snapdragon to interface with many commerically available ESC's.              


## How do I get set up? ##

* Checkout snap into the ROS workspace on a Snapdragon flight board
* Checkout [acl_msgs](https://bitbucket.org/brettlopez/acl_msgs) into the ROS workspace on the Snapdragon flight board and any local machine that needs access to the data from snap. acl_msgs contains custom ROS messages used by snap. NOTE: not required for ACL internal
* Open a terminal and build 
```
catkin_make
```
* Open a terminal and start Qualcomm's IMU application
```
imu_app -s 2
```
* Open another terminal and launch the stack
```
roslaunch snap snap.launch quad:="<vehicle name>"
```
* Open another terminal to start the [Motor interface](https://bitbucket.org/brettlopez/snap-dspal/overview). WARNING: this will allow the motor commands to be sent directly to ESC's. 
```
./motor_interface
```

## Inputs/Outputs ##
### Published Topics ###
 * "<vehicle name>"/imu    - IMU data
 * "<vehicle name>"/state  - Vehicle's full state
 * "<vehicle name>"/motors - Motor commands
 * "<vehicle name>"/smc    - Attitude controller data
 
### Subscribed Topics ###
 * "<vehicle name>"/pose     - Position and orientation update for estimator
 * "<vehicle name>"/attCmds  - Desired attitude and aungular rates
 
NOTE: The estimator uses a NWU coordinate system so the pose must be defined in the same frame. The pose should be transformed using the pose_relay.py script (in the scripts directory). There are already two examples in pose_relay.py that show how to do the transformation.   
 
## Custom messages ##
snap requires a number of custom ROS messages, which are found in [acl_msgs](https://bitbucket.org/brettlopez/acl_msgs). acl_msgs must be on the Snapdragon flight board and on other computers that use the messages. The acl_msgs package can be re-named to whatever the user prefers so long as the name change is reflected in the snap stack.

## Contribution guidelines ##

* Create a new branch if you want to add a new feature to the code base
```
git checkout -b <branch_name>
```
* Once the feature is complete, create a pull request for a senior student to review your work
```
git add <list files you want to commit>
git commit -m "<commit description>"
git push -u origin <branch_name>
```
* Go on bitbucket, and select "Create a pull request". Select your new branch, to be merged into the master branch, and write a description of what you changed. Submit the request.
* Use the issue tracker if a bug is found in the master branch

## Who do I talk to? ##

* Please contact Brett Lopez at btlopez@mit.edu
