#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseStamped
from acl_msgs.msg import ViconState


class Convert:
    def __init__(self):
        self.pubPose = rospy.Publisher("pose", PoseStamped, queue_size=1)
        self.pose = PoseStamped()

    def sendPose(self):
        self.pose.header.stamp = rospy.get_rostime()
        self.pubPose.publish(self.pose)


    def viconCB(self,data):
        self.pose.pose = data.pose
        self.sendPose()

    def poseCB(self,data):
        # Convert to right coodrinate frame
        self.pose.pose.position.x =  data.pose.position.x
        self.pose.pose.position.y = -data.pose.position.y
        self.pose.pose.position.z = -data.pose.position.z
        self.pose.pose.orientation.w =  data.pose.orientation.w
        self.pose.pose.orientation.x =  data.pose.orientation.x
        self.pose.pose.orientation.y = -data.pose.orientation.y
        self.pose.pose.orientation.z = -data.pose.orientation.z
        self.sendPose()

def startNode():
    c = Convert()
    rospy.Subscriber("vicon",ViconState,c.viconCB)
    #rospy.Subscriber("~/vislam/pose",PoseStamped,c.poseCB)
    rospy.spin()

if __name__ == '__main__':

    ns = rospy.get_namespace()
    try:
        rospy.init_node('relay')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
        else:
            print "Starting pose relay node for: " + ns
            startNode()
    except rospy.ROSInterruptException:
        pass
