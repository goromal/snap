/****************************************************************************
 *   Copyright (c) 2017 Brett T. Lopez. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name SND nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <mutex>
#include <cstdint>
#include <math.h>
#include <pthread.h>


typedef struct __attribute__ ((packed)) sMotorpacket
{ 
  pthread_mutex_t ipc_mutex;
  pthread_cond_t  ipc_condvar;
  uint16_t seq;
  uint16_t f[8];
} tMotorpacket;

struct Quaternion {
	float x = 0.0;
	float y = 0.0;
	float z = 0.0;
	float w = 1.0;
 } ;

struct Vector {
	float x = 0.0;
	float y = 0.0;
	float z = 0.0;
 } ;

struct desiredAttState {
	uint16_t attStatus;
	float throttle;
	Quaternion q ;
	Vector w;

	desiredAttState(){
		attStatus = 0;
		throttle = 0.0;
	}
} ;

struct attState {
	Quaternion q;
	Vector w;
} ;

// Integrator
struct Integrator
{
  double value;

  // Constructors:
  Integrator()
  {
    value = 0;
  }

  // Methods
  void increment(double inc, double dt)
  {
    value += inc * dt;
  }
  void reset()
  {
    value = 0;
  }

};