/****************************************************************************
 *   Copyright (c) 2017 Brett T. Lopez. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name snap nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#pragma once
#include <atomic>
#include <mutex>
#include <cstdint>
#include <math.h>

#ifndef STRUCT_
#define STRUCT_
#include "structs.h"
#endif

namespace Snapdragon {
  class ControllerManager;
}

class Snapdragon::ControllerManager {
public:

	typedef struct {
	    float Kp[3] = {0.4,0.4,0.4};
	    float Kd[3] = {0.15,0.15,0.15};
	    float Ki[3] = {0.01,0.01,0.01};	

	    float controlDT = 0.002;

	    // TODO: use these
	    float J[3] = {0.01,0.01,0.01};
	    float b    = 0.055;
	    float l    = 0.15;
	    float c    = 0.2;
	    
	    // Robust controller params (not currently being used)
	    float L[3] = {200.0,200.0,200.0};
	    float K[3] = {75.0,75.0,75.0};
	    float P[3] = {5.0,5.0,5.0};	    

	} InitParams;


	typedef struct {
	    float f[8] = {0.,0.,0.,0.,0.,0.,0.,0.};
	} motorForces;

	struct controlData {
	    Quaternion q_des ;
	    Quaternion q_act ;
	    Quaternion q_err ;
	    Vector w_des ;
	    Vector w_act ;
	    Vector w_err ;
	    Vector s ;
	} ;

	/**
	* Constructor
	**/
	ControllerManager();

	/**
	* Initalizes the Controller Manager with Controller Parameters
	* @param params
	*  The structure that holds the Controller parameters.
	* @return 
	*  0 = success
	* otherwise = failure.
	**/
	void Initialize
	( 
	const Snapdragon::ControllerManager::InitParams& params
	);

	void updateDesiredAttState( desiredAttState &desState, desiredAttState newdesState);
	void updateAttState( attState &attState, Quaternion q, Vector w);
	void updateMotorCommands ( Snapdragon::ControllerManager::motorForces &forces, desiredAttState desState, attState attState );

	/**
	* Destructor
	*/
	virtual ~ControllerManager();

	desiredAttState  smc_des_;
	attState         smc_state_;
	Snapdragon::ControllerManager::motorForces      forces_;
	Snapdragon::ControllerManager::controlData		smc_data_;
	Integrator rI,pI,yI;

private:
	std::atomic<bool> initialized_;
	std::atomic<bool> arm_;
	Snapdragon::ControllerManager::InitParams smc_params_;
	std::mutex                    sync_mutex_;
};
